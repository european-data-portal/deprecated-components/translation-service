
package eu.europa.ec.dgt.mtatec;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.ec.dgt.mtatec.types.InboundSimpleProxyMessage;


/**
 * <p>Java class for asktranslation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="asktranslation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://mtatec.dgt.ec.europa.eu/types}inboundSimpleProxyMessage"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "asktranslation", propOrder = {
    "arg0"
})
public class Asktranslation {

    @XmlElement(required = true)
    protected InboundSimpleProxyMessage arg0;

    /**
     * Gets the value of the arg0 property.
     * 
     * @return
     *     possible object is
     *     {@link InboundSimpleProxyMessage }
     *     
     */
    public InboundSimpleProxyMessage getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link InboundSimpleProxyMessage }
     *     
     */
    public void setArg0(InboundSimpleProxyMessage value) {
        this.arg0 = value;
    }

}
