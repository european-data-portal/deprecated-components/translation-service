package de.fhg.fokus.edp.translation.ftp;

import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.ReceiveBuilder;
import de.fhg.fokus.edp.translation.message.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.ftpserver.DataConnectionConfigurationFactory;
import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.Ftplet;
import org.apache.ftpserver.listener.ListenerFactory;
import scala.PartialFunction;
import scala.runtime.BoxedUnit;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by aos on 5/13/15.
 */
public class FtpServerActor extends AbstractActor {
    private LoggingAdapter log = Logging.getLogger(context().system(), this);

    private FtpServer server = null;
    private FtpFileSystemFactory fileSystemFactory = new FtpFileSystemFactory();
    private FtpUserManager userManager = new FtpUserManager();

    @Override
    public void preStart() throws Exception {
        context().system().eventStream().subscribe(self(), WorkerInit.class);
        context().system().eventStream().subscribe(self(), WorkerFailure.class);
        context().system().eventStream().subscribe(self(), WorkerDone.class);

        log.error("Hey this is: " + self().path());
        self().tell("start", self());
    }

    @Override
    public void postStop() throws Exception {
        log.debug("Shutting down ftp server");
        System.err.println("Shutting down ftp server");
        if (server != null && !server.isStopped()) {
            server.stop();
        }

        super.postStop();
    }

    public PartialFunction<Object, BoxedUnit> receive() {
        return ReceiveBuilder
                .match(FtpDocument.class, this::onFtpDocument)
                .match(WorkerInit.class, this::onWorkerInit)
                .match(WorkerDone.class, this::onWorkerDone)
                .match(WorkerFailure.class, this::onWorkerFailure)
                .match(String.class, (s) -> s.equals("start"), this::startFtp)
                .build();
    }


    private void startFtp(String s) throws FtpException {
        log.error("Got start ftp: from: " + sender());
        FtpServerFactory factory = new FtpServerFactory();
        ListenerFactory listenerFactory = new ListenerFactory();
        listenerFactory.setServerAddress(context().system().settings().config().getString("translation.ftp.listen.host"));
        listenerFactory.setPort(context().system().settings().config().getInt("translation.ftp.listen.port"));

        DataConnectionConfigurationFactory dataFactory = new DataConnectionConfigurationFactory();
        dataFactory.setActiveEnabled(false);
        dataFactory.setIdleTime(10);
        dataFactory.setPassiveAddress(context().system().settings().config().getString("translation.ftp.listen.host"));
        dataFactory.setPassiveExternalAddress(context().system().settings().config().getString("translation.ftp.external-address"));
        dataFactory.setPassivePorts(context().system().settings().config().getString("translation.ftp.listen.passive-ports"));
        listenerFactory.setDataConnectionConfiguration(dataFactory.createDataConnectionConfiguration());

        factory.addListener("default", listenerFactory.createListener());
        factory.setUserManager(userManager);
        factory.setFileSystem(fileSystemFactory);

        Map<String, Ftplet> ftplets = new HashMap<>();
        ftplets.put("default", new FtpCommandFilter(log));
        factory.setFtplets(ftplets);

        server = factory.createServer();
        server.start();
    }

    private void onFtpDocument(FtpDocument ftpDocument) {
        // Store the document with our filesystemview for the sender's (worker actor's) user.
        fileSystemFactory.getFileSystemByActorRef(sender()).setDocument(ftpDocument);

    }

    private void onWorkerInit(WorkerInit workerInit) {
        // Init & tell worker about his ftp account.
        String userName = RandomStringUtils.randomAlphanumeric(15);
        String password = RandomStringUtils.randomAlphanumeric(15);
        String host = context().system().settings().config().getString("translation.ftp.external-address") + ":"
                + context().system().settings().config().getInt("translation.ftp.listen.port");

        sender().tell(new FtpSetup(userName, password, host), self());

        FtpUser user = userManager.addUser(userName, password, workerInit.actor);
        fileSystemFactory.create(user);
    }

    private void onWorkerFailure(WorkerFailure workerFailure) {
        // Remove filesystemview for this user.
        fileSystemFactory.delete(workerFailure.actor);

        // Remove user instance from usermanager.
        userManager.delete(workerFailure.actor);
    }

    private void onWorkerDone(WorkerDone workerDone) {
        // FIXME Clear document for filesystemview.
    }
}
