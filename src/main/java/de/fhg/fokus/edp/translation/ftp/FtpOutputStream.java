package de.fhg.fokus.edp.translation.ftp;

import akka.actor.ActorRef;
import de.fhg.fokus.edp.translation.message.FtpDocument;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by aos on 5/15/15.
 */
public class FtpOutputStream extends ByteArrayOutputStream {
    private final ActorRef actorRef;
    private final String fileName;
    private boolean closed = false;

    public FtpOutputStream(String fileName, ActorRef actorRef) {
        this.fileName = fileName;
        this.actorRef = actorRef;
    }

    @Override
    public void close() throws IOException {
        if (!closed) {
            this.closed = true;
            actorRef.tell(new FtpDocument(fileName, toString()), null);
        }
    }
}
