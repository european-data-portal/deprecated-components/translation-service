package de.fhg.fokus.edp.translation;

import de.fhg.fokus.edp.translation.model.TranslationRequestBatch;

import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class ServiceState implements Serializable {
    protected List<TranslationRequestBatch> processing = new ArrayList<>();
    protected Deque<TranslationRequestBatch> queue = new ArrayDeque<>();
}