package de.fhg.fokus.edp.translation.worker;

import akka.actor.AbstractLoggingFSM;
import akka.actor.ActorSelection;
import akka.actor.Props;
import akka.dispatch.OnSuccess;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import de.fhg.fokus.edp.translation.message.*;
import de.fhg.fokus.edp.translation.model.TranslationRequest;
import de.fhg.fokus.edp.translation.model.TranslationRequestBatch;
import de.fhg.fokus.edp.translation.model.TranslationResponseBody;
import eu.europa.ec.dgt.mtatec.InboundSimpleConnectorService;
import eu.europa.ec.dgt.mtatec.InboundSimpleConnectorService_Service;
import eu.europa.ec.dgt.mtatec.types.InboundSimpleProxyMessage;
import kamon.Kamon;
import kamon.metric.instrument.Counter;
import kamon.metric.instrument.Histogram;
import kamon.metric.instrument.Memory;
import kamon.metric.instrument.Time;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.oasis.xliff.*;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.DetailEntry;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPFaultException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static akka.pattern.Patterns.ask;
import static org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString;

/**
 * Created by aos on 5/7/15.
 */
public class WorkerActor extends AbstractLoggingFSM<WorkerState, WorkerData> {

    private final LoggingAdapter log = Logging.getLogger(context().system(), this);

    private final Counter callbackScheduledCounter;
    private final Counter callbackCompletedCounter;
    private final Counter callbackErrorCounter;
    private final Histogram callbackDuration;
    private final Histogram batchTotalDuration;
    private final Histogram batchUpstreamDuration;
    private final Histogram batchDocumentSize;
    private final Counter batchUpstreamAccepted;
    private final Counter batchUpstreamError;
    private final Counter batchUpstreamDone;
    private final Counter batchPartialReceived;

    private final FiniteDuration translationTimeoutInterval;

    private final Marshaller xliffMarshaller;
    private final Unmarshaller xliffUnmarshaller;

    private final ActorSelection ftpActor;
    private final InboundSimpleConnectorService wsdlService;


    private FtpSetup ftpSetup = null;


    WorkerActor(ActorSelection ftpActor) throws JAXBException {

        // Setup Kamon counters
        callbackScheduledCounter = Kamon.metrics().counter("callback-scheduled");
        callbackCompletedCounter = Kamon.metrics().counter("callback-completed");
        callbackErrorCounter = Kamon.metrics().counter("callback-error");
        callbackDuration = Kamon.metrics().histogram("callback-duration");

        batchDocumentSize = Kamon.metrics().histogram("batch-document-size", Memory.Bytes());
        batchTotalDuration = Kamon.metrics().histogram("batch-total-duration");
        batchUpstreamAccepted = Kamon.metrics().counter("batch-upstream-accepted");
        batchUpstreamError = Kamon.metrics().counter("batch-upstream-error");
        batchUpstreamDone = Kamon.metrics().counter("batch-upstream-done");
        batchUpstreamDuration = Kamon.metrics().histogram("batch-upstream-duration", Time.Milliseconds());
        batchPartialReceived = Kamon.metrics().counter("batch-upstream-partial");

        // Setup JAX-WS
        wsdlService = new InboundSimpleConnectorService_Service().getInboundSimpleConnectorServicePort();
        BindingProvider bp = (BindingProvider) wsdlService;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
                context().system().settings().config().getString("translation.endpoint"));

        translationTimeoutInterval = Duration.apply(context().system().settings().config().getDuration("translation.translation-timeout", TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS);

        JAXBContext xliffContext = JAXBContext.newInstance(Xliff.class.getPackage().getName());
        xliffMarshaller = xliffContext.createMarshaller();
        xliffUnmarshaller = xliffContext.createUnmarshaller();

        this.ftpActor = ftpActor;


        startWith(WorkerState.WAIT, Uninitialized.UNINITIALIZED);

        when(WorkerState.WAIT, Duration.apply(20, TimeUnit.SECONDS),
                matchEventEquals(StateTimeout(), (evt, data) -> {
                    context().parent().tell(new WorkerIdle(), self());
                    return goTo(WorkerState.IDLE).using(Uninitialized.UNINITIALIZED);
                })
                        .event(TranslationError.class, TranslationProcessing.class, (err, processing) -> {
                            log.error("Received translation error in WAIT. Batch size: {} Error: {}", processing.batchSize(), err);

                            StringBuilder builder = new StringBuilder();
                            builder.append("Upstream callback error. What?");
                            builder.append("\n\nBatch size: " + processing.batchSize());
                            builder.append("\nERROR message: " + err);
                            builder.append("\n\n\n");
                            builder.append("\n\n\n");

                            builder.append(getXliffDocument(processing.getBatch()));

                            context().parent().tell(new WorkerFailure(new Exception(builder.toString()), self()), self());

                            return stay();
                        })
        );

        when(WorkerState.IDLE, matchEvent(TranslationRequestBatch.class, (batch, any) -> {
            return translate(batch);
        }));

        when(WorkerState.CALLOUT, translationTimeoutInterval,
                matchEvent(TranslationError.class, TranslationProcessing.class, (err, processing) -> {
                    // If only a single language has failed, mark that and continue processing.
                    if (StringUtils.countMatches(err.targetLanguage, ',') > 0) {
                        // > 1 language has failed => regard this batch as a failure.
                        batchUpstreamError.increment();
                        log.error("Received translation error. Batch size: {} Error: {}", processing.batchSize(), err);

                        StringBuilder builder = new StringBuilder();
                        builder.append("Upstream callback error. What?");
                        builder.append("\n\nBatch size: " + processing.batchSize());
                        builder.append("\nERROR message: " + err);
                        builder.append("\n\n\n");
                        builder.append("\n\n\n");

                        builder.append(getXliffDocument(processing.getBatch()));

                        context().parent().tell(new WorkerFailure(new Exception(builder.toString()), self()), self());

                        return goTo(WorkerState.WAIT);
                    }

                    log.error("Partial failure. " + err);

                    processing.getBatch().getTargetLanguages().remove(err.targetLanguage.toLowerCase());
                    if (processing.getTargetLanguagesCount() == processing.getTranslatedLanguagesCount()) {
                        // FIXME: Is a success sent from MT@EC if the failed language was the last remaining one?
                        log.info("We have a languages! Process :" + processing.toString());
                        TranslationResult success = new TranslationResult(processing.getBatch().getUniqueId());
                        return onSuccess(success, processing);
                    }
                    return stay();


                }).event(FtpDocument.class, TranslationProcessing.class, (document, processing) -> {
                    return onFtpDocument(processing, document);
                }).eventEquals(StateTimeout(),(evt,data) -> {
                    log.info("Our waiting time for another FTP Document is gone. Lets take what we have");
                    log.info("DataObject :" + data.toString());
                    TranslationProcessing processing = (TranslationProcessing) data;
                    TranslationResult success = new TranslationResult(processing.getBatch().getUniqueId());
                    return onSuccess(success,processing);

                    /*context().parent().tell(new WorkerFailure(new Exception("Translation timeout while processing... must've been so long after the last message but some languages maybe be translated " +
                            translationTimeoutInterval), self()), self());*/

                    //return goTo(WorkerState.WAIT);
                })
        );

        when(WorkerState.CALLBACK,
                matchEvent(TranslationResult.class, TranslationProcessing.class, (result, processing) -> onSuccess(result, processing)));

        ask(ftpActor, new WorkerInit(self()), 5000 /*ms*/).onSuccess(new OnSuccess() {
            @Override
            public void onSuccess(Object result) throws Throwable {
                ftpSetup = (FtpSetup) result;
                log.debug("Received FtpSetup: " + ftpSetup);
                initialize();
            }
        }, context().system().dispatcher());
    }

    public static Props props(ActorSelection ftpActor) {
        return Props.create(WorkerActor.class, () -> new WorkerActor(ftpActor));
    }

    private State<WorkerState, WorkerData> onFtpDocument(TranslationProcessing processing, FtpDocument ftpDocument) throws JAXBException {
        log.debug("Received FtpDocument after " + processing.timeElapsed() + "ms: " + ftpDocument);
        if (!processing.getBatch().getUniqueId().equals(ftpDocument.getReceivedId())) {
            log.error("Received file did not match expected payload id. got {} expected {}", ftpDocument.getReceivedId(), processing.getBatch().getUniqueId());
            return stay();
        }

        batchPartialReceived.increment();

        StringReader reader = new StringReader(ftpDocument.getBody());
        Xliff xliff;

        try {
            xliff = (Xliff) xliffUnmarshaller.unmarshal(reader);
        } catch (JAXBException ex) {
            batchUpstreamError.increment();
            log.error("Received invalid XML! Batch size: {} Error: {}", processing.batchSize(), ex);

            StringBuilder builder = new StringBuilder();
            builder.append("Invalid XML. Huh?");
            builder.append("\n\nBatch size: " + processing.batchSize());
            builder.append("\nERROR message: " + ex);
            builder.append("\n\n\n");
            builder.append("\n\n\n");
            builder.append(ftpDocument.getBody());
            builder.append("\n\n\n");
            builder.append("\n\n\n");
            builder.append("\n\n\n");

            builder.append(getXliffDocument(processing.getBatch()));

            context().parent().tell(new WorkerFailure(new Exception(builder.toString()), self()), self());

            return goTo(WorkerState.WAIT);

        } catch (Exception e) {
            throw e;
        }

        File xliffFile = (File) xliff.getAnyAndFile().get(0);
        for (Object go : xliffFile.getBody().getGroupOrTransUnitOrBinUnit()) {
            Map<String, String> units = new HashMap<>();
            Group g = (Group) go;

            for (Object o : g.getGroupOrTransUnitOrBinUnit()) {

                TransUnit tu = (TransUnit) o;
                StringBuilder translatedString = new StringBuilder();

                for (Object c : tu.getTarget().getContent()) {
                    if (c instanceof Mrk) {

                        Mrk mrk = (Mrk) c;
                        for (Object s : ((Mrk) c).getContent()) {
                            translatedString.append((String) s);
                            translatedString.append(" ");
                        }
                    } else if (c instanceof String) {
                        translatedString.append((String) c);
                    }
                }

                units.put(tu.getId(), translatedString.toString());
            }

            if (processing.providesTranslationFor(g.getId()))
                processing.getTranslationFor(g.getId()).put(ftpDocument.getLang().toLowerCase(), units);
            else {
                log.error("Batch: " + processing.getBatch().getUniqueId() + " received translation for " + ftpDocument.getLang() + " but contained invalid key: " + g.getId());
            }
        }

        reader.close();

        log.error("Batch: " + processing.getBatch().getUniqueId() + " has " + processing.getTargetLanguagesCount() + " target languages and " + processing.getTranslatedLanguagesCount() + " translated ones");
        if (processing.getTargetLanguagesCount() == processing.getTranslatedLanguagesCount()) {
            // Fix for missing HTTP callback.

            TranslationResult success = new TranslationResult(processing.getBatch().getUniqueId());
            return onSuccess(success, processing);

            // return goTo(WorkerState.CALLBACK).using(processing);
        } else return stay();
    }

    /**
     * Fires off the callback if sufficient translation confirmations have been received for each target language.
     *
     * @param success
     * @param state
     * @throws JsonProcessingException
     */
    protected State<WorkerState, WorkerData> onSuccess(TranslationResult success, TranslationProcessing state) {
        if (!(state.getBatch().getUniqueId().equals(success.id))) {
            // FIXME unspecified behaviour on the MT@EC side?
            log.error("state.getBatch().getUniqueId() " + state.getBatch().getUniqueId() + " does not equal success.id " + success.id);
            return stay();
        }

        // FIXME we assume that if #translations == #target_languages all datasets are translated.
        // FIXME we should treat TranslationResult properly and take into account the targetLanguage parameter therein

        batchUpstreamDone.increment();
        batchUpstreamDuration.record(state.timeElapsed());
        log.info("Translation batch finished! " + state.getBatch().getUniqueId() + " total size: " + state.batchSize() + " total time : " + state.timeElapsed());

        // XXX - we drop failed requests. this might not be intended behaviour.
        callbackScheduledCounter.increment(state.batchSize());
        final long callbackStart = System.currentTimeMillis();

        List<Pair<TranslationRequest, Exception>> failedRequests = new ArrayList<>();

        for (TranslationRequest req : state.getBatch().getQueue()) {

            TranslationResponseBody response = null;

            if (state.getTranslationFor(req.callback.payload.id) != null) {
                response = new TranslationResponseBody(req.callback.payload.id, req.originalLanguage, state.getTranslationFor(req.callback.payload.id));
            } else {
                continue;
            }

            try {
                HttpResponse<String> postResponse = Unirest.post(req.callback.url)
                        .headers(req.callback.headers)
                        .header("Content-Type", "application/json")
                        .body(response)
                        .asString();

                if ((postResponse.getStatus() / 100) == 2)
                    callbackCompletedCounter.increment();
                else {
                    callbackErrorCounter.increment();
                    failedRequests.add(Pair.of(req, new Exception(postResponse.getStatus() + " - " + postResponse.getStatusText())));
                }
            } catch (UnirestException e) {
                callbackErrorCounter.increment();
                failedRequests.add(Pair.of(req, e));
                e.printStackTrace();
            }
        }

        // FIXME make use of onTransition!?
        long delta = (System.currentTimeMillis() - callbackStart);
        if (!(failedRequests.size() == state.batchSize())) {
            callbackDuration.record(delta);
            batchTotalDuration.record(state.timeElapsed());
        }

        if (failedRequests.size() == 0) {
            log.debug("Completed " + (state.batchSize() - failedRequests.size()) + " callbacks successfully! Woohoo! Just took us " + delta + "ms");
            context().system().eventStream().publish(new WorkerDone(self()));
        } else {
            log.error("Callback failed at least partially. Reporting to parent. Total time (ms): " + delta);
            StringBuilder builder = new StringBuilder();
            builder.append("Batch size: " + state.batchSize());
            builder.append("\nSuccessful callbacks: " + (state.batchSize() - failedRequests.size()));
            builder.append("\nERROR callbacks: ");
            for (Pair<TranslationRequest, Exception> p : failedRequests) {
                builder.append("\n " + p.getRight().toString() + " ::: " + p.getLeft());
            }

            builder.append("\n\n\n");

            context().parent().tell(new WorkerFailure(new Exception(builder.toString()), self()), self());
        }
        return goTo(WorkerState.WAIT);
    }

    /**
     * Provides the Xliff document for the MT@EC service
     *
     * @return Xliff document
     */
    protected String getXliffDocument(TranslationRequestBatch batch) throws JAXBException {
        StringWriter out = new StringWriter();

        Xliff xliff = new Xliff();
        xliff.setVersion("1.2");
        File xliffFile = new File();
        xliffFile.setOriginal(batch.getBatchId() + ".json");
        xliffFile.setDatatype("plaintext");
        xliffFile.setSourceLanguage(batch.getOriginalLanguage());
        Body body = new Body();

        for (TranslationRequest req : batch.getQueue()) {
            Group g = new Group();
            g.setId(req.callback.payload.id);

            for (Map.Entry<String, String> e : req.dataDict.entrySet()) {
                TransUnit u = new TransUnit();
                u.setId(e.getKey());
                org.oasis.xliff.Source s = new org.oasis.xliff.Source();
                s.setLang(batch.getOriginalLanguage());
                s.getContent().add(e.getValue());
                u.setSource(s);

                g.getGroupOrTransUnitOrBinUnit().add(u);

            }
            body.getGroupOrTransUnitOrBinUnit().add(g);
        }

        xliffFile.setBody(body);
        xliff.setLang(batch.getOriginalLanguage());
        xliff.getAnyAndFile().add(xliffFile);

        xliffMarshaller.marshal(xliff, out);
        String outString = out.toString();

        batchDocumentSize.record(outString.getBytes().length);


        return outString;
    }

    /**
     * Returns the callback URL for the MT@EC service
     *
     * @param type callback placeholder
     * @return
     */
    protected String getCallbackUrl(String id, String type) {

        String path = encodeBase64URLSafeString(self().path().toSerializationFormat().getBytes());

        return context().system().settings().config().getString("translation.url")
                + "/" + type + "?"
                + "worker=" + path
                + "&id=" + id;
    }

    protected String getFTPUrl() {
        return "ftp://" + ftpSetup.username + ":" + ftpSetup.password + "@" + ftpSetup.host + "/abc.xlf";
    }

    private void sendXliffperEmail(FtpDocument ftpDocument, InboundSimpleProxyMessage msg) throws EmailException {
                Email email = new SimpleEmail();
                email.setDebug(context().system().settings().config().getBoolean("translation.mail.debug"));
                email.setHostName(context().system().settings().config().getString("translation.mail.server"));
                int port = context().system().settings().config().getInt("translation.mail.port");
                if (context().system().settings().config().getBoolean("translation.mail.ssl")) {
                    email.setSSLOnConnect(true);
                    email.setSslSmtpPort("" + port);
                } else {
                    email.setSSLOnConnect(false);
                    email.setSmtpPort(port);
                }

                if (context().system().settings().config().getBoolean("translation.mail.auth")) {
                    email.setAuthentication(context().system().settings().config().getString("translation.mail.username"),
                            context().system().settings().config().getString("translation.mail.password"));
                }
                for (String address : context().system().settings().config().getStringList("translation.mail.xlifrec"))
                    email.addTo(address);

                email.setFrom(context().system().settings().config().getString("translation.mail.sender"), "EDP Translation service");
                email.setSubject("XliffDocument");

                email.setMsg("This Email contains the Xliff File" + ftpDocument.toString() + "The String of Soap msg send to MC@EC: " + msg.toString());

                //log.info("Sending failure email for batch. Total size: " + failedBatch.size());
                 email.send();
    }

    /**
     * Inbound translation currentBatch handler.
     * <p/>
     * As we're running on the same VM we need to forcibly clone the currentBatch. Else, both the service actor & this worker will
     * have access to the same TranslationRequestBatch instance which means that if the result callback fails, the persisted
     * currentBatch as part of the StateUpdate will contain the updated translation fields.
     * The consequence of this is that StateUpdate(remove=true) won't work properly unless we use {@link SerializationUtils#clone}
     *
     * @param batch
     */
    protected State<WorkerState, WorkerData> translate(TranslationRequestBatch batch) throws JAXBException {
        InboundSimpleProxyMessage msg = new InboundSimpleProxyMessage();
        msg.setRequesterCallback(getCallbackUrl(batch.getUniqueId(), "result"));
        msg.setErrorCallback(getCallbackUrl(batch.getUniqueId(), "error"));

        msg.setExternalReference(self().path().toSerializationFormat());
        msg.setSourceLanguage(batch.getOriginalLanguage());
        msg.setTargetLanguage(StringUtils.join(batch.getTargetLanguages(), ","));
        // We've "hacked" our FTP server so that the document is always returned...
        msg.setTargetTranslationPath(getFTPUrl());
        msg.setDocumentToTranslate(getFTPUrl());
        msg.setPriority(1);
        msg.setUsername(context().system().settings().config().getString("translation.wsdl.username"));
        msg.setApplicationName(context().system().settings().config().getString("translation.wsdl.application-name"));

        msg.setExternalReference(batch.getUniqueId());
        msg.setOriginalFileName(batch.getUniqueId());
        msg.setOutputFormat("xliff");
        msg.setRequestType("doc");
        msg.setDomains("all");

        log.debug("Result callback is at: " + getCallbackUrl(batch.getUniqueId(), "result"));
        log.debug("FTPUrl is: " + getFTPUrl());

        // FIXME we should await a reply before continuing.
        FtpDocument ftpDocument = new FtpDocument("/abc.xlf", getXliffDocument(batch));
        ftpActor.tell(ftpDocument, self());

        if (!context().system().settings().config().getBoolean("translation.mock")) {
            try {
                sendXliffperEmail(ftpDocument,msg);
                String translationId = wsdlService.asktranslation(msg);
                batchUpstreamAccepted.increment();
                return goTo(WorkerState.CALLOUT).using(new TranslationProcessing(batch, translationId));
            } catch (SOAPFaultException ex) {

                log.error(ex.toString());
                log.error("SOAPFaultException.Detail: " + ex.getFault().getDetail());
                log.error("This is the FTP Document which failed: " +ftpDocument.toString());
                log.error("This is the message: " + msg.toString());
                Iterator it = ex.getFault().getDetail().getDetailEntries();

                while (it.hasNext()) {
                    DetailEntry entry = (DetailEntry) it.next();
                    log.error("SOAPFaultException.Detail.[]: " + entry);
                }
                batchUpstreamError.increment();

                ex.printStackTrace();
            } catch (Exception e) {
                batchUpstreamError.increment();
                e.printStackTrace();
            }
        } else {
            TranslationProcessing state = new TranslationProcessing(batch, "b47ch-" + Math.random());
            String sourceLanguage = batch.getOriginalLanguage();

            for (TranslationRequest req : batch.getQueue()) {
                for (String targetLanguage : batch.getTargetLanguages()) {
                    Map<String, String> fakeTranslation = new HashMap<String, String>();
                    for (Map.Entry<String, String> e : req.dataDict.entrySet()) {
                        fakeTranslation.put(e.getKey(),
                                "MOCKUP[" + targetLanguage + "] ::: " + e.getValue());
                    }

                    state.getTranslationFor(req).put(targetLanguage, fakeTranslation);
                }
            }

            long delay = (long) (Math.random() * 300);
            log.info("Mockup translation done. Delaying callbacks: " + delay + "s");

            context().system().scheduler().scheduleOnce(
                    Duration.apply(delay, TimeUnit.SECONDS),
                    self(),
                    new TranslationResult(batch.getUniqueId()),
                    context().dispatcher(), self()
            );

            return goTo(WorkerState.CALLBACK).using(state);
        }
        return goTo(WorkerState.WAIT);
    }

}
