package de.fhg.fokus.edp.translation.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by aos on 5/7/15.
 */
public class TranslationRequestCallback implements Serializable {
    public String url;
    public String method;
    public TranslationRequestPayload payload;
    public Map<String, String> headers;

    @Override
    public String toString() {
        return "TranslationRequestCallback{" +
                "url='" + url + '\'' +
                ", method='" + method + '\'' +
                ", payload=" + payload +
                ", headers=" + headers +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof TranslationRequestCallback)) return false;

        TranslationRequestCallback that = (TranslationRequestCallback) o;

        return new EqualsBuilder()
                .append(url, that.url)
                .append(method, that.method)
                .append(payload, that.payload)
                .append(headers, that.headers)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(url)
                .append(method)
                .append(payload)
                .append(headers)
                .toHashCode();
    }
}
