package de.fhg.fokus.edp.translation.model;

/**
 * Created by aos on 5/7/15.
 */


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class TranslationRequest implements Serializable {
    public List<String> languages;
    public TranslationRequestCallback callback;

    @JsonProperty("original_language")
    public String originalLanguage;
    @JsonProperty("data_dict")
    public Map<String, String> dataDict;


    @JsonIgnore
    public final String getBatchId() {
        return originalLanguage + "::" + StringUtils.join('-', languages);
    }

    public String getId() {
        return callback.payload.id;
    }

    @Override
    public String toString() {
        return "TranslationRequest{" +
                "languages=" + languages +
                ", callback=" + callback +
                ", originalLanguage='" + originalLanguage + '\'' +
                //", dataDict=" + dataDict +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TranslationRequest that = (TranslationRequest) o;

        return new EqualsBuilder()
                .append(languages, that.languages)
                .append(callback, that.callback)
                .append(originalLanguage, that.originalLanguage)
                .append(dataDict, that.dataDict)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(languages)
                .append(callback)
                .append(originalLanguage)
                .append(dataDict)
                .toHashCode();
    }
}
