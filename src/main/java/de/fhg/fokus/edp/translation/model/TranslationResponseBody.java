package de.fhg.fokus.edp.translation.model;

import java.util.Map;

/**
 * Created by aos on 5/7/15.
 */
public class TranslationResponseBody {
    public String id;
    public String originalLanguage;
    public Map<String, Map<String, String>> translation;

    public TranslationResponseBody(String id, String originalLanguage, Map<String, Map<String, String>> translation) {
        this.id = id;
        this.originalLanguage = originalLanguage;
        this.translation = translation;
    }
}
