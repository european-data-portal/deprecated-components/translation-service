package de.fhg.fokus.edp.translation;

import akka.actor.ActorRef;
import akka.actor.Cancellable;
import akka.actor.OneForOneStrategy;
import akka.actor.SupervisorStrategy;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.ReceiveBuilder;
import akka.persistence.AbstractPersistentActor;
import akka.persistence.SaveSnapshotSuccess;
import akka.persistence.SnapshotOffer;
import akka.persistence.SnapshotSelectionCriteria;
import de.fhg.fokus.edp.translation.message.StateUpdate;
import de.fhg.fokus.edp.translation.message.WorkerDone;
import de.fhg.fokus.edp.translation.message.WorkerFailure;
import de.fhg.fokus.edp.translation.message.WorkerIdle;
import de.fhg.fokus.edp.translation.model.TranslationRequest;
import de.fhg.fokus.edp.translation.model.TranslationRequestBatch;
import de.fhg.fokus.edp.translation.worker.WorkerActor;
import kamon.Kamon;
import kamon.metric.instrument.Counter;
import kamon.metric.instrument.Gauge;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import scala.PartialFunction;
import scala.concurrent.duration.Duration;
import scala.runtime.BoxedUnit;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static akka.dispatch.Futures.future;

/**
 * Created by aos on 5/7/15.
 */
public class ServiceActor extends AbstractPersistentActor {

    private LoggingAdapter log = Logging.getLogger(context().system(), this);
    private Deque<ActorRef> idleWorkers = new ArrayDeque<>();
    private HashMap<ActorRef, TranslationRequestBatch> processingWorkers = new HashMap<>();
    private Cancellable tick;
    private ServiceState state = new ServiceState();

    private Counter batchStartedCounter = Kamon.metrics().counter("batch-started");
    private Counter batchFailedCounter = Kamon.metrics().counter("batch-failed");
    private Counter batchFinishedCounter = Kamon.metrics().counter("batch-finished");
    private Counter workerCrashCounter = Kamon.metrics().counter("worker-crash");
    private Gauge batchRemainingGauge = Kamon.metrics().gauge("batch-remaining", Duration.create(30, TimeUnit.SECONDS), () -> state.queue.size());
    private Gauge batchRemainingSizeGauge = Kamon.metrics().gauge("batch-remaining-size", Duration.create(30, TimeUnit.SECONDS), () ->
            state.queue.stream().mapToInt(TranslationRequestBatch::size).sum());

    private Gauge workerCountGauge = Kamon.metrics().gauge("worker-count", Duration.create(1, TimeUnit.SECONDS), () -> processingWorkers.size());

    private SupervisorStrategy strategy = new OneForOneStrategy(-1, Duration.Inf(),
            t -> {
                log.debug("Worker failed! Killing it!");
                workerCrashCounter.increment();

                if (idleWorkers.contains(sender())) idleWorkers.remove(sender());
                context().system().eventStream().publish(new WorkerFailure(t, sender()));

                return SupervisorStrategy.restart();
            });

    @Override
    public PartialFunction<Object, BoxedUnit> receiveRecover() {
        return ReceiveBuilder
                .match(StateUpdate.class, s -> {
                    if (s.remove) {
                        // FIXME check here?
                        state.queue.remove(s.batch);
                    } else state.queue.add(s.batch);
                })
                .match(SnapshotOffer.class, this::recoverFromSnapshot)
                .build();
    }

    @Override
    public PartialFunction<Object, BoxedUnit> receiveCommand() {
        return ReceiveBuilder
                .match(TranslationRequestBatch.class, this::requestBatchTranslation)
                .match(WorkerIdle.class, this::workerIdle)
                .match(WorkerFailure.class, this::workerFailed)
                .match(WorkerDone.class, this::workerDone)
                .match(String.class, s -> s.equals("start-workers"), s -> this.startWorkers())
                .match(String.class, s -> s.equals("snapshot"), s -> {
                    log.debug("Taking snapshot.");
                    saveSnapshot(state);
                })
                .match(String.class, s -> s.equals("get-processing-ids"), s -> {
                    List<String> ids = Stream.concat(state.queue.stream(), processingWorkers.values().stream()).flatMap(b -> b.getQueue().stream()).map(TranslationRequest::getId).collect(Collectors.toList());
                    sender().tell(ids, self());
                })
                .match(SaveSnapshotSuccess.class, s -> {
                    log.debug("Snapshot saved successfully. Deleting old messages.");
                    deleteSnapshots(SnapshotSelectionCriteria.create(s.metadata().sequenceNr() - 1, s.metadata().timestamp()));
                    deleteMessages(s.metadata().sequenceNr());
                })
                .build();
    }


    /**
     * To avoid having to re-read the whole journal upon each application start we set up a tick for taking a
     * snapshot of our current state every 24h.
     */
    @Override
    public void preStart() throws Exception {
        super.preStart();

        context().system().eventStream().subscribe(self(), WorkerDone.class);
        context().system().eventStream().subscribe(self(), WorkerFailure.class);

        long snapshotInterval = context().system().settings().config().getDuration("translation.snapshot-interval", TimeUnit.HOURS);

        tick = getContext().system().scheduler().schedule(
                Duration.create(snapshotInterval, TimeUnit.HOURS),
                Duration.create(snapshotInterval, TimeUnit.HOURS),
                self(), "snapshot", getContext().dispatcher(), null);

        self().tell("start-workers", null);
    }

    @Override
    public void postStop() {
        super.postStop();
        if (tick != null)
            tick.cancel();
    }

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return strategy;
    }


    protected void recoverFromSnapshot(SnapshotOffer snapshot) {
        state = (ServiceState) snapshot.snapshot();

        for (TranslationRequestBatch batch : state.processing) {
            self().tell(batch, null);
        }

        state.processing.clear();
    }

    /**
     * This method is called at preStart via a message sent to self.
     */
    protected void startWorkers() {
        int workerCount = getContext().system().settings().config().getInt("translation.worker-count");

        log.info("Booting up {} workers.", workerCount);

        for (int i = 0; i < workerCount; i++) {
            context().actorOf(WorkerActor.props(context().system().actorSelection("user/ftp")));
        }

    }

    /**
     * Handler called when a worker has successfully finished processing a translation batch and fired the callback.
     *
     * @param done
     */
    private void workerDone(WorkerDone done) {
        if (processingWorkers.containsKey(done.actor)) {
            batchFinishedCounter.increment();
            TranslationRequestBatch processed = processingWorkers.remove(done.actor);

            persist(new StateUpdate(processed, true), (s) -> {
                state.processing.remove(processed);
            });
        } else {
            log.error("Got worker done but worker {} was not doing any processing!", done.actor);
        }
    }

    /**
     * Method for handling translation failure. This can be either because the callback failed or because the worker crashed.
     * <p/>
     * We persist the failure so that the batch is not tried again accidentally upon server crash/restart.
     * Afterwards, we send an email to all addresses listed inside the application configuration file with a copy of the batch.
     * <p/>
     * FIXME Externalize email service? (use container configuration/JEE?!)
     * XXX - we discard failed batches. maybe we should reschedule them?
     *
     * @param failure
     */
    private void workerFailed(WorkerFailure failure) {
        TranslationRequestBatch failedBatch = processingWorkers.remove(failure.actor);

        if (failedBatch == null) return;
        batchFailedCounter.increment();

        state.processing.remove(failedBatch);
        persist(new StateUpdate(failedBatch, true), (evt) -> {
            log.error("A worker {} failed {} trying to process this batch: {}", failure.actor, failure.failure, failedBatch);

            future(() -> {
                Email email = new SimpleEmail();
                email.setDebug(context().system().settings().config().getBoolean("translation.mail.debug"));
                email.setHostName(context().system().settings().config().getString("translation.mail.server"));
                int port = context().system().settings().config().getInt("translation.mail.port");
                if (context().system().settings().config().getBoolean("translation.mail.ssl")) {
                    email.setSSLOnConnect(true);
                    email.setSslSmtpPort("" + port);
                } else {
                    email.setSSLOnConnect(false);
                    email.setSmtpPort(port);
                }

                if (context().system().settings().config().getBoolean("translation.mail.auth")) {
                    email.setAuthentication(context().system().settings().config().getString("translation.mail.username"),
                            context().system().settings().config().getString("translation.mail.password"));
                }
                for (String address : context().system().settings().config().getStringList("translation.mail.recipients"))
                    email.addTo(address);

                email.setFrom(context().system().settings().config().getString("translation.mail.sender"), "EDP Translation service");
                email.setSubject("Batch translation batch failed");

                email.setMsg("Dear owner,\n this is your obedient service actor.\n" +
                        "Unfortunately, one of my worker children (" + failure.actor + ") has died " +
                        "and I'd like to report the reason for its death.\n" + failure.failure + "\n\n\n" +
                        failedBatch.toString());

                log.info("Sending failure email for batch. Total size: " + failedBatch.size());

                if (context().system().settings().config().getBoolean("translation.mail.skip"))
                    return "skipped";
                else return email.send();
            }, context().system().dispatcher());
        });
    }

    /**
     * Handler for inbound translation requests.
     * If we have an available worker, the batch is forwarded and the worker removed from the list of available workers.
     * Else, we add the batch to the queue.
     * Either way, we persist the translation batch.
     *
     * @param batch
     */

    protected void requestBatchTranslation(TranslationRequestBatch batch) {
        persist(new StateUpdate(batch), (s) -> {
            if (idleWorkers.isEmpty()) {
                log.debug("Received translation batch, however all workers are busy at the moment.");
                state.queue.push(batch);
            } else {
                log.debug("Received translation batch, time to get that translation started!");
                ActorRef worker = idleWorkers.pop();
                onWorkerStartProcessing(worker, batch);
            }
        });
    }

    private void onWorkerStartProcessing(ActorRef worker, TranslationRequestBatch batch) {
        batchStartedCounter.increment();
        state.processing.add(batch);
        processingWorkers.put(worker, batch);

        worker.tell(batch, self());
    }

    /**
     * Handler called when worker is finished with a previous batch or was newly created.
     * If the worker was previously processing a batch, we purge said batch from our state.
     * Furthermore, if there's available work, the worker is then directed to work instead of being added to the list
     * of available workers.
     *
     * @param msg ignored
     */
    protected void workerIdle(WorkerIdle msg) {
        // FIXME Better check? What if worker is in idle & processing?
        if (!idleWorkers.contains(sender())) {

            log.debug("Got an idle worker {}", sender().toString());

            if (state.queue.isEmpty()) {
                log.info("Worker {} requested translation but queue was empty. Current idle count: {}", sender().path().toStringWithoutAddress(), idleWorkers.size() + 1);
                idleWorkers.add(sender());
                return;
            }

            TranslationRequestBatch batch = state.queue.pop();
            onWorkerStartProcessing(sender(), batch);
        } else {
            log.info("Worker reported as idle but is already listed as idle. " + sender());
        }
    }

    @Override
    public String persistenceId() {
        return "batch-router";
    }
}
