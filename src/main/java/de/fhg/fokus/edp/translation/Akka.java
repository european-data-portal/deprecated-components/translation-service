package de.fhg.fokus.edp.translation;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.google.inject.Singleton;
import de.fhg.fokus.edp.translation.batch.TranslationRequestRouter;
import de.fhg.fokus.edp.translation.ftp.FtpServerActor;
import org.jboss.resteasy.logging.Logger;

/**
 * Created by aos on 5/7/15.
 */

@Singleton
public class Akka {
    private final static Logger log = Logger.getLogger(Akka.class);
    public final ActorSystem system;
    public final ActorRef service;
    public final ActorRef batcher;
    public final ActorRef ftp;

    public Akka() {
        log.debug("" + getClass().getClassLoader().getResource("."));
        system = ActorSystem.create("translation");

        batcher = system.actorOf(TranslationRequestRouter.props(), "batch-router");

        ftp = system.actorOf(Props.create(FtpServerActor.class), "ftp");
        service = system.actorOf(Props.create(ServiceActor.class), "service");

        log.info("Initialized Akka.");
    }
}
