package de.fhg.fokus.edp.translation.message;

/**
 * Created by aos on 5/7/15.
 */
public class TranslationResult {
    public String id;
    public String targetLanguage = null, requestId = null;

    public TranslationResult(String id, String targetLanguage, String requestId) {
        this.id = id;
        this.targetLanguage = targetLanguage;
        this.requestId = requestId;
    }

    public TranslationResult(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "TranslationResult{" +
                "id='" + id + '\'' +
                ", targetLanguage='" + targetLanguage + '\'' +
                ", requestId='" + requestId + '\'' +
                '}';
    }
}
