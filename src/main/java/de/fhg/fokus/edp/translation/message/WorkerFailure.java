package de.fhg.fokus.edp.translation.message;

import akka.actor.ActorRef;

/**
 * Created by aos on 5/13/15.
 */
public class WorkerFailure {
    public final Throwable failure;
    public final ActorRef actor;

    public WorkerFailure(Throwable failure, ActorRef actor) {
        this.failure = failure;
        this.actor = actor;
    }
}
