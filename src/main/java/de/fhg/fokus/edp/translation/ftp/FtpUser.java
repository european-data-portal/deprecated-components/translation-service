package de.fhg.fokus.edp.translation.ftp;

import akka.actor.ActorRef;
import com.google.common.collect.Lists;
import org.apache.ftpserver.ftplet.Authority;
import org.apache.ftpserver.ftplet.AuthorizationRequest;
import org.apache.ftpserver.ftplet.User;
import org.apache.ftpserver.usermanager.impl.ConcurrentLoginRequest;
import org.apache.ftpserver.usermanager.impl.WritePermission;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aos on 5/15/15.
 */
public class FtpUser implements User {
    private final String name, password;
    private final ActorRef ref;

    private List<Authority> authorities = Lists.newArrayList(new WritePermission());

    public FtpUser(String name, String password, ActorRef ref) {
        this.name = name;
        this.password = password;
        this.ref = ref;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public List<Authority> getAuthorities() {
        return authorities;
    }

    @Override
    public List<Authority> getAuthorities(Class<? extends Authority> aClass) {
        return authorities;
    }

    @Override
    public AuthorizationRequest authorize(AuthorizationRequest authorizationRequest) {
        return authorizationRequest;
    }

    @Override
    public int getMaxIdleTime() {
        return 60;
    }

    @Override
    public boolean getEnabled() {
        return true;
    }

    public ActorRef getActorRef() {
        return ref;
    }

    @Override
    public String getHomeDirectory() {
        return "/tmp/data";
    }
}
