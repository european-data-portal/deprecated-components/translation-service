package de.fhg.fokus.edp.translation.message;

import akka.actor.ActorRef;

/**
 * Created by aos on 5/15/15.
 */
public class WorkerInit {
    public ActorRef actor;

    public WorkerInit(ActorRef actor) {
        this.actor = actor;
    }
}
