package de.fhg.fokus.edp.translation.message;

/**
 * Created by aos on 5/15/15.
 */
public class FtpSetup {
    public String username, password, host;

    public FtpSetup(String username, String password, String host) {
        this.username = username;
        this.password = password;
        this.host = host;
    }

    @Override
    public String toString() {
        return "FtpSetup{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", host='" + host + '\'' +
                '}';
    }

}
