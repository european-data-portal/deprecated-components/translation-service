package de.fhg.fokus.edp.translation.batch;

import akka.actor.AbstractLoggingFSM;
import akka.actor.FSM;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import de.fhg.fokus.edp.translation.ScalaHelpers;
import de.fhg.fokus.edp.translation.model.TranslationRequest;
import de.fhg.fokus.edp.translation.model.TranslationRequestBatch;
import kamon.Kamon;
import kamon.metric.instrument.Counter;
import kamon.metric.instrument.Histogram;
import kamon.metric.instrument.Time;
import scala.collection.immutable.Map;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by aos on 9/23/15.
 */
public class TranslationRequestBatcher extends AbstractLoggingFSM<BatchState, BatchData> {
    private int BATCH_SIZE = 50;
    private LoggingAdapter log = Logging.getLogger(context().system(), this);
    private Histogram durationHistogram = null;
    private Histogram sizeHistogram = null;
    private Counter batchIdCounter = null;
    private Counter batchElementCounter = null;

    private long startTime;

    // XXX - we can't persist unfortunately without writing at least some Scala. blame Java!
    TranslationRequestBatcher(String batchId) {
        BATCH_SIZE = context().system().settings().config().getInt("translation.batch-size");
        FiniteDuration batchTimeout =
                Duration.apply(context().system().settings().config().getDuration("translation.batch-timeout", TimeUnit.MINUTES), TimeUnit.MINUTES);
        startWith(BatchState.IDLE, Uninitialized.UNINITIALIZED);

        when(BatchState.IDLE, Duration.apply(2, TimeUnit.MINUTES),
                matchEvent(TranslationRequest.class, Uninitialized.class,
                        (req, uninitialized) -> {
                            startTime = System.currentTimeMillis();
                            TranslationRequestBatch batch = new TranslationRequestBatch(req);

                            batchElementCounter.increment();

                            return goTo(BatchState.ACTIVE).using(batch);
                        })
                        .eventEquals(StateTimeout(), (evt, data) -> stop())
        );

        when(BatchState.ACTIVE, batchTimeout,
                matchEvent(TranslationRequest.class, TranslationRequestBatch.class,
                        (req, data) -> {
                            if (!data.isCompatible(req))
                                throw new RuntimeException("Incompatible request. batchId: " + data.getBatchId() + " -- " + req.getBatchId());

                            batchElementCounter.increment();
                            data.add(req);

                            if (data.size() >= BATCH_SIZE)
                                return flush(data);
                            else return stay().using(data);
                        }).

                        eventEquals(StateTimeout(), TranslationRequestBatch.class,
                                (event, data) -> flush(data)));


        initialiseKamon(batchId);
        initialize();

    }

    public static Props props(String batchId) {
        return Props.create(TranslationRequestBatcher.class, () -> new TranslationRequestBatcher(batchId));
    }

    private void initialiseKamon(String batchId) {
        HashMap<String, String> tags = new HashMap<>();
        tags.put("batch-id", batchId);

        Map<String, String> tagsScala = ScalaHelpers.toScalaMap(tags);

        durationHistogram = Kamon.metrics().histogram("batch-collection-duration", tagsScala, Time.Milliseconds());
        sizeHistogram = Kamon.metrics().histogram("batch-collection-size", tagsScala);
        batchIdCounter = Kamon.metrics().counter("batch-collection-id", tagsScala);
        batchElementCounter = Kamon.metrics().counter("batch-collection-input", tagsScala);
    }

    private FSM.State<BatchState, BatchData> flush(TranslationRequestBatch data) {
        long durationDelta = System.currentTimeMillis() - startTime;
        log.info("Batch complete. Batch id: " + data.getBatchId() + " Batch size: " + data.size() + " Batch duration: " + durationDelta);

        durationHistogram.record(durationDelta);
        sizeHistogram.record(data.size());
        batchIdCounter.increment();

        context().system().actorSelection("user/service").tell(data, self());
        return goTo(BatchState.IDLE).using(Uninitialized.UNINITIALIZED);
    }
}
