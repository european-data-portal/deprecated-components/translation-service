package de.fhg.fokus.edp.translation;

import kamon.Kamon;
import sun.security.ssl.SSLSocketFactoryImpl;

import javax.net.ssl.HttpsURLConnection;
import javax.ws.rs.core.Application;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by aos on 4/29/15.
 */
public class TranslationApplication extends Application {

    private final HashSet<Class<?>> classes = new HashSet<>();
    private final HashSet<Object> singletons = new HashSet<>();

    public TranslationApplication() {
        classes.add(ServiceResource.class);
    }

    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
