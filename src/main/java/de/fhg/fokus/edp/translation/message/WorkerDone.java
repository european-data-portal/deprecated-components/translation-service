package de.fhg.fokus.edp.translation.message;

import akka.actor.ActorRef;

/**
 * Created by aos on 5/15/15.
 */
public class WorkerDone {
    public ActorRef actor;

    public WorkerDone(ActorRef actor) {
        this.actor = actor;
    }
}
