package de.fhg.fokus.edp.translation.batch;

/**
 * Created by aos on 9/23/15.
 */
public enum BatchState {
   IDLE, ACTIVE
}
