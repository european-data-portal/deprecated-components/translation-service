package de.fhg.fokus.edp.translation.ftp;

import akka.event.LoggingAdapter;
import com.google.common.collect.Lists;
import org.apache.ftpserver.ftplet.*;

import java.io.IOException;
import java.util.List;

/**
 * @author Andreas C. Osowski
 */
public class FtpCommandFilter implements Ftplet {
    private final LoggingAdapter log;

    public FtpCommandFilter(LoggingAdapter log) {
        this.log = log;
    }

    private List<String> permittedCommands = Lists.newArrayList(
            "USER", "PASS", "STOR", "TYPE", "PASV", "MKD", "CWD", "QUIT", "RETR", "SYST", "SITE", "CHMOD"
    );

    @Override
    public void init(FtpletContext ftpletContext) throws FtpException {
    }

    @Override
    public void destroy() {
    }

    @Override
    public FtpletResult beforeCommand(FtpSession ftpSession, FtpRequest ftpRequest) throws FtpException, IOException {
        if (permittedCommands.contains(ftpRequest.getCommand()))
            return FtpletResult.DEFAULT;
        else {
            log.error("Received invalid command {} from: {}", ftpRequest.getCommand(), ftpSession.getClientAddress());
            return FtpletResult.DISCONNECT;
        }
    }

    @Override
    public FtpletResult afterCommand(FtpSession ftpSession, FtpRequest ftpRequest, FtpReply ftpReply) throws FtpException, IOException {
        return FtpletResult.DEFAULT;
    }

    @Override
    public FtpletResult onConnect(FtpSession ftpSession) throws FtpException, IOException {
        return FtpletResult.DEFAULT;
    }

    @Override
    public FtpletResult onDisconnect(FtpSession ftpSession) throws FtpException, IOException {
        return FtpletResult.DEFAULT;
    }
}
