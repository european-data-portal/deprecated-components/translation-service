package de.fhg.fokus.edp.translation.ftp;

import akka.actor.ActorRef;
import org.apache.ftpserver.ftplet.*;
import org.apache.ftpserver.usermanager.UsernamePasswordAuthentication;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by aos on 5/15/15.
 */
public class FtpUserManager implements UserManager {

    private HashMap<String, FtpUser> users = new HashMap<>();


    @Override
    public User getUserByName(String s) {
        if (users.containsKey(s))
            return users.get(s);
        return null;
    }

    @Override
    public String[] getAllUserNames() throws FtpException {
        return users.keySet().toArray(new String[]{});
    }

    @Override
    public void delete(String s) throws FtpException {
    }

    @Override
    public void save(User user) throws FtpException {
    }

    @Override
    public boolean doesExist(String s) throws FtpException {
        return getUserByName(s) != null;
    }

    @Override
    public User authenticate(Authentication authentication) throws AuthenticationFailedException {
        if (!(authentication instanceof UsernamePasswordAuthentication)) return null;
        UsernamePasswordAuthentication auth = (UsernamePasswordAuthentication) authentication;


        User u = getUserByName(auth.getUsername());
        if (u != null) {
            if (u.getPassword().equals(auth.getPassword()))
                return u;
        }
        return null;
    }

    @Override
    public String getAdminName() throws FtpException {
        return "";
    }

    @Override
    public boolean isAdmin(String s) throws FtpException {
        return s.isEmpty();
    }

    public void delete(ActorRef actor) {
        for (Map.Entry<String, FtpUser> entry : users.entrySet()) {
            if (entry.getValue().getActorRef() == actor) {
                users.remove(entry.getKey());
                return;
            }
        }
    }

    public FtpUser addUser(String username, String password, ActorRef actorRef) {
        FtpUser user = new FtpUser(username, password, actorRef);
        users.put(username, user);
        return user;
    }
}
