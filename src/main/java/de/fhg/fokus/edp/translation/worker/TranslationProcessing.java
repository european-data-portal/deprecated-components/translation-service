package de.fhg.fokus.edp.translation.worker;

import de.fhg.fokus.edp.translation.model.TranslationRequest;
import de.fhg.fokus.edp.translation.model.TranslationRequestBatch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by aos on 10/9/15.
 */
public class TranslationProcessing implements WorkerData {
    private final TranslationRequestBatch batch;
    private final Map<String, Map<String, Map<String, String>>> translations;
    private final long translationStartTime;
    private final String translationId;
    private int translatedLanguagesCount;


    public TranslationProcessing(TranslationRequestBatch batch, String translationId) {
        this.batch = batch;
        this.translationStartTime = System.currentTimeMillis();
        this.translationId = translationId;

        translations = new HashMap<>();
        for (TranslationRequest req : batch.getQueue())
            translations.put(req.callback.payload.id, new HashMap<>());
    }

    public TranslationRequestBatch getBatch() {
        return batch;
    }

    public List<TranslationRequest> getRequests() {
        return batch.getQueue();
    }

    public long timeElapsed() {
        return System.currentTimeMillis() - translationStartTime;
    }

    public Map<String, Map<String, String>> getTranslationFor(TranslationRequest req) {
        return getTranslationFor(req.callback.payload.id);
    }

    public Map<String, Map<String, String>> getTranslationFor(String id) {
        return translations.get(id);
    }

    public int getTranslatedLanguagesCount() {
        return getTranslationFor(batch.getQueue().get(0)).size();
    }

    public int getTargetLanguagesCount() {
        return batch.getTargetLanguages().size();
    }

    public int batchSize() {
        return getBatch().size();
    }

    public boolean providesTranslationFor(String id) {
        return translations.containsKey(id);
    }
}
