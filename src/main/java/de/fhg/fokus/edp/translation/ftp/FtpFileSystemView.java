package de.fhg.fokus.edp.translation.ftp;

import de.fhg.fokus.edp.translation.message.FtpDocument;
import org.apache.ftpserver.ftplet.FileSystemView;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.FtpFile;

/**
 * Created by aos on 5/15/15.
 */
public class FtpFileSystemView implements FileSystemView {
    private final FtpUser user;
    private FtpDocument document;
    private FtpFakeFile file = new FtpFakeFile(this, "/");

    public FtpFileSystemView(FtpUser user) {
        this.user = user;
    }

    @Override
    public FtpFile getHomeDirectory() throws FtpException {
        return file;
    }

    @Override
    public FtpFile getWorkingDirectory() throws FtpException {
        return file;
    }

    @Override
    public boolean changeWorkingDirectory(String s) throws FtpException {
        return true;
    }

    @Override
    public FtpFile getFile(String s) throws FtpException {
        return new FtpFakeFile(this, s);
    }

    @Override
    public boolean isRandomAccessible() throws FtpException {
        return true;
    }

    @Override
    public void dispose() {

    }

    public void setDocument(FtpDocument document) {
        this.document = document;
    }

    public FtpDocument getDocument() {
        return document;
    }

    public FtpUser getUser() {
        return user;
    }
}
