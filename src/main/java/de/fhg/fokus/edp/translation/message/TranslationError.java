package de.fhg.fokus.edp.translation.message;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * Created by aos on 5/7/15.
 */
public class TranslationError implements Serializable {
    public final int errorCode;
    public final String errorMessage;
    public final String targetLanguage;
    public final String id;

    public TranslationError(String id, int errorCode, String errorMessage, String targetLanguage) {
        this.id = id;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.targetLanguage = targetLanguage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TranslationError that = (TranslationError) o;

        return new EqualsBuilder()
                .append(errorCode, that.errorCode)
                .append(errorMessage, that.errorMessage)
                .append(targetLanguage, that.targetLanguage)
                .append(id, that.id)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(errorCode)
                .append(errorMessage)
                .append(targetLanguage)
                .append(id)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "TranslationError{" +
                "errorCode=" + errorCode +
                ", errorMessage='" + errorMessage + '\'' +
                ", targetLanguage='" + targetLanguage + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
