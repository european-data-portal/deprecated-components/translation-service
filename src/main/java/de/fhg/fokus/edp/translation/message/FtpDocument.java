package de.fhg.fokus.edp.translation.message;

import org.apache.commons.io.FilenameUtils;

/**
 * Created by aos on 5/15/15.
 */
public class FtpDocument {
    private final String body;
    private final String fileName;
    private final String pathName;
    private final String lang;
    private final String receivedId;

    public FtpDocument(String pathName, String body) {
        this.pathName = pathName;
        this.body = body;

        fileName = FilenameUtils.getBaseName(pathName);
        lang = fileName.substring(fileName.length() - 2);
        receivedId = fileName.substring(0, fileName.length() - 3);
    }

    public String getBody() {
        return body;
    }

    public String getFileName() {
        return fileName;
    }

    public String getPathName() {
        return pathName;
    }

    public String getLang() {
        return lang;
    }

    public String getReceivedId() {
        return receivedId;
    }

    @Override
    public String toString() {
        return "FtpDocument{" +
                "fileName='" + fileName + '\'' +
                ", pathName='" + pathName + '\'' +
                ", lang='" + lang + '\'' +
                ", receivedId='" + receivedId + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
