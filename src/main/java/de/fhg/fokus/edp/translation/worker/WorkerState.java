package de.fhg.fokus.edp.translation.worker;

/**
 * Created by aos on 10/9/15.
 */
public enum WorkerState {
    IDLE, CALLOUT, CALLBACK, WAIT;
}
