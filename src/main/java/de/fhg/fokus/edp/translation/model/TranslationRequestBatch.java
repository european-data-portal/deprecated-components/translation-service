package de.fhg.fokus.edp.translation.model;

import com.google.common.collect.Lists;
import de.fhg.fokus.edp.translation.batch.BatchData;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by aos on 9/23/15.
 */
public class TranslationRequestBatch implements BatchData, Serializable {
    private final List<TranslationRequest> queue;
    private final String originalLanguage;
    private final List<String> targetLanguages;
    private final String callbackUrl;
    private final String batchId;
    private final String uuid = UUID.randomUUID().toString();


    public TranslationRequestBatch(TranslationRequest request) {
        queue = Lists.newArrayList(request);
        originalLanguage = request.originalLanguage.toLowerCase();
        targetLanguages = request.languages.stream().map((s) -> s.toLowerCase()).collect(Collectors.toList());
        callbackUrl = request.callback.url;
        batchId = request.getBatchId();
    }

    public TranslationRequestBatch add(TranslationRequest request) {
        queue.add(request);

        return this;
    }

    public int size() {
        return queue.size();
    }

    public List<TranslationRequest> getQueue() {
        return queue;
    }

    public String getBatchId() {
        return batchId;
    }

    public String getUniqueId() {
        // MT@EC caches translation results. We need to circumvent that.
        return uuid;
    }

    public boolean isCompatible(TranslationRequest request) {
        return batchId.equalsIgnoreCase(request.getBatchId());
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public List<String> getTargetLanguages() {
        return targetLanguages;
    }

    @Override
    public String toString() {
        return "TranslationRequestBatch{" +
                "queue=" + queue +
                ", originalLanguage='" + originalLanguage + '\'' +
                ", targetLanguages=" + targetLanguages +
                ", callbackUrl='" + callbackUrl + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TranslationRequestBatch that = (TranslationRequestBatch) o;

        return new EqualsBuilder()
                .append(queue, that.queue)
                .append(originalLanguage, that.originalLanguage)
                .append(targetLanguages, that.targetLanguages)
                .append(callbackUrl, that.callbackUrl)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(queue)
                .append(originalLanguage)
                .append(targetLanguages)
                .append(callbackUrl)
                .toHashCode();
    }
}
