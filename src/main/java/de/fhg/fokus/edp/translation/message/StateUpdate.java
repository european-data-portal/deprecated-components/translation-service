package de.fhg.fokus.edp.translation.message;

import de.fhg.fokus.edp.translation.model.TranslationRequestBatch;

import java.io.Serializable;

/**
 * Created by aos on 5/13/15.
 */
public class StateUpdate implements Serializable {
    public boolean remove = false;
    public TranslationRequestBatch batch;

    public StateUpdate(TranslationRequestBatch batch) {
        this.batch = batch;
    }

    public StateUpdate(TranslationRequestBatch batch, boolean remove) {
        this.batch = batch;
        this.remove = remove;
    }

    @Override
    public String toString() {
        return "StateUpdate{" +
                "remove=" + remove +
                ", batch=" + batch +
                '}';
    }
}
