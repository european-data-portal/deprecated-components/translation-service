//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.04.15 at 02:17:08 PM CEST 
//


package org.oasis.xliff;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for context-typeValueList.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="context-typeValueList">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="database"/>
 *     &lt;enumeration value="element"/>
 *     &lt;enumeration value="elementtitle"/>
 *     &lt;enumeration value="linenumber"/>
 *     &lt;enumeration value="numparams"/>
 *     &lt;enumeration value="paramnotes"/>
 *     &lt;enumeration value="record"/>
 *     &lt;enumeration value="recordtitle"/>
 *     &lt;enumeration value="sourcefile"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "context-typeValueList")
@XmlEnum
public enum ContextTypeValueList {


    /**
     * Indicates a database content.
     * 
     */
    @XmlEnumValue("database")
    DATABASE("database"),

    /**
     * Indicates the content of an element within an XML document.
     * 
     */
    @XmlEnumValue("element")
    ELEMENT("element"),

    /**
     * Indicates the name of an element within an XML document.
     * 
     */
    @XmlEnumValue("elementtitle")
    ELEMENTTITLE("elementtitle"),

    /**
     * Indicates the line number from the sourcefile (see context-type="sourcefile") where the <source> is found.
     * 
     */
    @XmlEnumValue("linenumber")
    LINENUMBER("linenumber"),

    /**
     * Indicates a the number of parameters contained within the <source>.
     * 
     */
    @XmlEnumValue("numparams")
    NUMPARAMS("numparams"),

    /**
     * Indicates notes pertaining to the parameters in the <source>.
     * 
     */
    @XmlEnumValue("paramnotes")
    PARAMNOTES("paramnotes"),

    /**
     * Indicates the content of a record within a database.
     * 
     */
    @XmlEnumValue("record")
    RECORD("record"),

    /**
     * Indicates the name of a record within a database.
     * 
     */
    @XmlEnumValue("recordtitle")
    RECORDTITLE("recordtitle"),

    /**
     * Indicates the original source file in the case that multiple files are merged to form the original file from which the XLIFF file is created. This differs from the original <file> attribute in that this sourcefile is one of many that make up that file.
     * 
     */
    @XmlEnumValue("sourcefile")
    SOURCEFILE("sourcefile");
    private final String value;

    ContextTypeValueList(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ContextTypeValueList fromValue(String v) {
        for (ContextTypeValueList c: ContextTypeValueList.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
