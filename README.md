## Synopsis

`translation-service` is a batching proxy for the European Commission's MT@EC machine translation service built upon akka, kamon, Jetty and the JVM. For providing access to the batched translation requests to MT@EC, a mock FTP server is provided.

## Code Example

Currently, the project deploys to to `http://localhost:8080/translation-service/`, an example translation request can be found below which could be `POST`ed to `http://localhost:8080/translation-service/`:

```json
{
  "languages": ["de","fr"],
  "original_language": "en",
  "callback": {
    "url": "http://httpbin.org/post",
    "method": "POST",
    "headers": {
      "X-Authentication-Token": "d34db33f",
    },
    "data_dict": {
      "hello_world": "Hello, world!",
      "hay": "How are you?"
    },
    "payload": {
      "id": "this-is-a-sample-id"
    }
  }
}
```

A POST request containing the translation would look similar to the one below:
```json
{
  "id": "this-is-a-sample-id",
  "original_language": "en",
  "translation": {
    "de": {
      "hello_world": "Hallo, Welt!",
      "hay": "Wie geht es Ihnen?"
    },
    "fr": {
      "hello_world": "Bonjour, le monde!",
      "hay": "Comment allez-vous?"
    }
  }
}
```
## Motivation

The MT@EC service can only ingest translation requests as a regular string or by retrieving a file from a FTP server. Furthermore, it is currently quite slow with per-request translation times of about 10 minutes (to 24 languages). To speed up translation of our CKAN documents, we decided to implement a proxy that 
* batches inbound translation requests by their source and target languages
* provides access to the batches via a mock FTP server

## Installation

The project can easily be packaged as an executable jar by running `mvn compile package`. The Jarfile currently starts a Jetty web server on port 8080. Prior to packaging, however, it is recommended to adapt the configuration files in `src/main/resources/application.conf` and `src/main/resources/env.conf` to the deployment environment. Information on how to obtain access to the MT@EC service can be founde at [MT@EC](http://ec.europa.eu/dgs/translation/translationresources/machine_translation/index_en.htm)

For configuring [kamon](http://kamon.io) please see the official kamon documentation at http://kamon.io/introduction/overview/.

Below is a short overview of important configuration settings:
* `translation.url` contains the external URL at which the translation service can be reached for HTTP callbacks from MT@EC
* `translation.ftp` configures the mock FTP server
  * `translation.ftp.external-address` DNS name or IP for connecting to the FTP server
  * `translation.ftp.listen` configures which IP address and ports the FTP server should listen on and use for file transfers
* `translation.mail` configures the mail server to use for notifying the owner of failed batches.
* `translation.mock` configures whether the service should mock translation responses and skip the MT@EC calls.

## API Reference

**TODO**

## Tests

**TODO**

## License
Licensed under the terms of the [EUPL v1.1](https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11)
See [LICENSE.pdf](LICENSE.pdf)
